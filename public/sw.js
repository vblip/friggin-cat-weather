// Names of the two caches used in this version of the service worker.
// Change to v2, etc. when you update any of the local resources, which will
// in turn trigger the install event again.
const APP_NAME = 'Friggin Cat Weather'
const NOTIFY_INTERVAL = 15

const VERSION = 'v1'
const PRECACHE = ['2-precache', VERSION].join('-')
const RUNTIME = ['1-runtime', VERSION].join('-')

const Service = self
const UPDATE_ACTION = 'update'
const LEARN_ACTION = 'learn'

// A list of local resources we always want to be cached.
const PRECACHE_URLS = [
  './', // Precache all 200 pages are good... find a way to ignore cache on bad 200's
  'client.js',
  'quotes.js',
  'homescreen192.png',
  'manifest.json',
  'cache/keep.jpg',
]

const RECACHE_MIMES = [
  "text/html",
  "text/css",
  "application/javascript"
]

const transformContent = {
  start() {}, // req, https://developer.mozilla.org/en-US/docs/Web/API/TransformStream
  async transform(chunkP, controller) {
    const chunk = await chunkP
    if (chunk === null) {
      console.log('Content transform complete.')
      controller.terminate()
    } else {
      const og = this.textDecoder.decode(chunk)
      const modified = og.replace(/21/g, '69')
      const outChunk = this.textEncoder.encode(modified)
      controller.enqueue(outChunk)
      // const outChunk = new Blob([modified], {type : 'text/html'})
      // controller.enqueue(outChunk)
      // controller.enqueue(modified)
    }
  },
  flush(controller) {
    console.log('Content transform flushing.')
    /* do any destructor work here */
  }
}

class sedDoc extends self.TransformStream {
  constructor() {
    const textEncoder = new TextEncoder()
    const textDecoder = new TextDecoder()
    super({ ...transformContent, textEncoder, textDecoder })
  }
}

// returns promise of response
async function fetchDoc (event, cachedResponse) {
  console.log(`[FetchDoc] URL ${event.request.url}.`)
  const indexResponse = (await caches.match(PRECACHE_URLS[0])) || {}
  return caches.open(RUNTIME).then(cache => {
    return fetch(event.request).then(response => {
      const headersI = response.headers // iterator
      const headersKVA = [...headersI.keys()].map(k => [k, headersI.get(k)])
      const headers = new Map(headersKVA)
      console.info('headers: ', headers)
 
      // To use a header... CORS whitelisting may be needed
      //   - Headers tend to get censored away at the fetch API level
      if(headersI.has("x-powered-by") && headersI.get("x-powered-by") === "Express") {
        console.log(`${APP_NAME}: Powered by Express`)
      } else if (cachedResponse) {
        console.log('Bad fetch, but cache came to the rescue!')
        return cachedResponse
      } else {
        console.log('We are in some deep shit, no cache, no valid fetch... wait?')
      }
      
      // if error response, use cached version
      if (response.status > 200 && cachedResponse) {
        console.info('response status is bad.')
        return cachedResponse
      }
      
      // todo: this seems unnecesary for fetchDoc
      console.log('SANITY CHECK')
      const readable = response.body
      const transformed = readable.pipeThrough(new sedDoc())
      const initR = {
        status: response.status,
        statusText: response.statusText,
        headers: Object.fromEntries(headers)
      }
      const myResponse = new Response(transformed, initR)
      try {
        cache.put(event.request, myResponse.clone())
      } catch (e) {
        console.warn('Could not save doc to cache.', e)
      }
      return myResponse
    }).catch(e => {
      if (cachedResponse)  { // pass the offline pwa test
        return cachedResponse
      } else {
        console.log(['Bad fetch... responding poorly. ',e])
        return e
      }
    })
  })
}


// returns promise of response
async function fetchAsset (event, cachedResponse) {
  console.debug(`[FetchAsset] triggered. URL ${event.request.url}.`)
  const indexResponse = (await caches.match(PRECACHE_URLS[0])) || {}
  return caches.open(RUNTIME).then(cache => {
    return fetch(event.request).then(response => {
      // Put a copy of the response in the runtime cache.
      // Booting is 201
      if (response.status > 200 && cachedResponse) {
        console.warn(`Asset Response bad! server is booting or refreshing. Using cache instead.`)
        return cachedResponse
      }
      if (indexResponse.url === response.url) {
        caches.open(PRECACHE).then(
          cache.put(event.request, response.clone())
        )
      }
      
      return cache.put(event.request, response.clone()).then(() => {
        return response
      })
    }).catch(e => {
      if (cachedResponse)  { // pass the offline pwa test
        return cachedResponse
      } else {
        console.log(['Bad fetch... responding poorly. ',e])
        return e
      }
    })
  })
}

// The install handler takes care of precaching the resources we always need.
self.addEventListener('install', event => {
  // self.skipWaiting(); https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/skipWaiting
  console.log('Install triggered.')
  event.waitUntil((async () => {
    await caches.open(RUNTIME)
    // If you get, Failed to execute 'Cache' on 'addAll': Request failed
    // That means one of the Precache URLs is wrong.
    const precache = await caches.open(PRECACHE)
    console.log('precache opened.')
    await precache.addAll(PRECACHE_URLS)
    console.log('precache complete.')
    self.skipWaiting() // Don't wait for all sessions to close
  })())
})

// This runs pretty often from what I can tell
// The activate handler takes care of cleaning up old caches.
self.addEventListener('activate', event => {
  console.log('Activate triggered.') // Runs post install, once per new service worker
  const currentCaches = [PRECACHE, RUNTIME]
  // const currentCaches = []; // Kills everything all the time
  
  // TODO: Cache cleanup should be done on waitUntil
  // By this point it's a bit too late
  event.waitUntil(async () => {
    const cacheNames = await caches.keys()
    const cachesToDelete = await cacheNames.filter(c => !currentCaches.includes(c))
    await Promise.all(cachesToDelete.map(cacheToDelete => {
      return caches.delete(cacheToDelete)
    }))
    self.clients.claim() // commandeer active sessions
  })
})

// The fetch handler serves responses for same-origin resources from a cache.
// If no response is found, it populates the runtime cache with the response
// from the network before returning it to the page.
self.addEventListener('fetch', event => {
  const lm = {
    cached: false
  }
  
  // Skip cross-origin requests, like those for Google Analytics.
  if (!event.request.url.startsWith(self.location.origin)) {
    return
  }
  
  event.respondWith(
    caches.match(event.request).then(cachedResponse => {
      if (cachedResponse) {
        lm.cached = true
      }

      if (event.request.destination === 'document') {
        console.info({ // todo: remove, precache seems to bypass this listener automatically
          installing: self.registration.installing,
          waiting: self.registration.waiting
        })
        return fetchDoc(event, cachedResponse)
      } else {
        return fetchAsset(event, cachedResponse) 
      }
    })
  )
})


/*
#### START NOTIFICATIONS CODE ####
TODO: MOVE TO A NATIVE WEB WORKER, a la fibonacci


function displayNotification(msg) {
  const title = 'There\'s an update to the T&D LMS'
  const options = {
    actions: [
      {
        action: UPDATE_ACTION,
        title: 'Update!',
        icon: '/demos/notification-examples/images/action-1-128x128.png'
      },
      {
        action: LEARN_ACTION,
        title: 'Do it',
        icon: '/demos/notification-examples/images/action-2-128x128.png'
      }
    ]
  };

  const maxVisibleActions = Notification.maxActions;
  if (maxVisibleActions < 2) {
    options.body = `${msg}\n${maxVisibleActions} actions.`;
  } else {
    options.body = `This notification can display up to ` +
      `${maxVisibleActions} actions.`;
  }
  
  if (Notification.permission == 'granted' && Math.random() < 0.001 ) {
    Service.registration.showNotification(title, options)
  }
}

async function bgSyncOcasionally() {
  if ('periodicSync' in Service.registration) {
    try {
      await Service.registration.periodicSync.register('content-sync', {
        // An interval of one day.
        minInterval: 2 * 60 * 60 * 1000,
      });
    } catch (error) {
      console.log('No BG Sync for you!')
    }
  } else {
    console.log('Why you no have BG sync... what\'s wrong with you!')
  }
}

self.onnotificationclick = function(event) {
  // event.notification.close() broken api?
  if (event.action === UPDATE_ACTION) {
    console.log('Update me senpai!')
  } else if (event.action === LEARN_ACTION) {
    bgSyncOcasionally()
    console.log('I wanna learn!')
  } else {
    // Main body of notification was clicked
    // self.clients.openWindow('/')
  }
}

// #### END NOTIFICATIONS CODE #### */
