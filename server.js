// server.js
// where your node app starts

import _ from './public/utils.js';

// init project
import express from 'express'
import { default as fs, promises} from 'fs'
import fetch from 'node-fetch'
import Jimp from 'jimp'
import { dirname } from 'path'

const { readFile, copyFile } = promises
const app = express()
const ctx = this

const cm = {
  lat: 29.75, // Defaults to Houston, TX
  long: -95.39,
  curse: 'Fückīng',
  // End of URL Search Query parameter support
  
  timeZone: 'America/Chicago', // 'America/Los_Angeles' or 'America/New_York' work too
  hour12: false,
  units: 'si',
  // End of reasonable options
  
  template: null,
  baseUrl: '/',
  indexTplFile: 'views/index.tmpl.html',
  basePublicDir: 'public',
  img: '', // leave blank for auto
  imgOfLastResort: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Felis_silvestris_silvestris_small_gradual_decrease_of_quality.png/200px-Felis_silvestris_silvestris_small_gradual_decrease_of_quality.png',
  noaaTemplate: {properties: {periods: [{},{}]}},
  defaultCat: 'keep.jpg',
  catTemplate: [{ url: '/cache/keep.jpg', id: '1234' }],
  catFilesDir: 'cache',
  catOpts: {headers: {'x-api-key': process.env.CAT_API_KEY}}
}

const BASE_PATH = dirname(process.argv[1])

// ###################### START CACHE CODE
const CACHE_LOCATION = 'location'
const CACHE_FORECAST = 'forecast'
const CACHE_CAT = 'cat'
const CACHE_CAT_FILE = 'catFile'

// TODO: Auto Expire these caches using a cache config
const caches = {
  location: {}, // "lat,long": valid Location response
  forecast: {}, // QUERY: valid noaa response
  cat: {}, // QUERY: valid noaa response
  catFile: {}
}

// Default retention is one hour
const getStamp = (retentionTimeInSeconds) => {
  const retention = Number.isInteger(retentionTimeInSeconds) ? retentionTimeInSeconds : 3600
  return '' + (
    (Date.now() / 1000 / retention) >> 0
  ) + ':'
}

const hashers = {
  location: (args) => '' + getStamp(0) + args.lat + ',' + args.long,
  forecast: (url) => getStamp(3600) + url,
  cat: (url) => getStamp(5) + url, // no more than one cat per 5 seconds
  catFile: (url) => getStamp(0) + url // Keep small cat files for ever
}

const getHash = (cacheName, rawKey) => hashers[cacheName](rawKey)
const setCache = (cacheName, rawKey, obj2Cache) => {
  if (!obj2Cache) { throw new Error('Value 2 cache is not an Object') }
  if (!caches[cacheName]) { throw new Error('Cache does not exists. Please initialized the cache.') }
  const hash = getHash(cacheName, rawKey)
  caches[cacheName][hash] = JSON.parse(JSON.stringify(obj2Cache))
  return caches[cacheName][hash]
}
const getCached = (cacheName, rawKey) => {
  if ( rawKey === null ) { throw new Error('getCached cache key is not defined!') }
  const hash = getHash(cacheName, rawKey)
  return caches[cacheName][hash] || null
}
const getCacheLength = (cacheName) => Object.keys(caches[cacheName]).length
const getLastCached = (cacheName, ...rest) => {
  if ( rest.length > 0 ) { throw new Error('getLastCached cache expects one argument.') }
  const keys = Object.keys(caches[cacheName])
  if ( keys.legnth === 0 ) return null
  const result = caches[cacheName][keys.pop()]
  return result
}
// ###################### END CACHE CODE

// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public/stylesheets'))
app.use(express.static('public/cache'))
app.use(express.static('public'))


// http://expressjs.com/en/starter/basic-routing.html
app.get('/', async function(request, response) {
  const sm = startTiming('/')
  response.header('rayf-socket-endpoint' , 'https://vblip.com');
  // _.log(request.headers)
  sm.acceptEncoding = request.headers['accept-encoding'] || ''
  Object.assign(sm, {
    lat: request.query.lat ? parseFloat(request.query.lat) : cm.lat,
    long: request.query.long ? parseFloat(request.query.long) : cm.long,
    curse: request.query.curse ? request.query.curse : cm.curse,
    
    template: '',
    noaaUrl: null,
    noaa: null,
    loc: null,
    cat: null,
    finalImageSrc: null,
    modernBrowser: Math.random() > 0.1 && sm.acceptEncoding.split(', ').includes('deflate'), // indicator of heavy duty browser
    promises: [],
    wfo: '', // grid authority
    x: '', // noaa grid points
    y: '',
  })
  
  if (cm.template) { // update cache if already set
    setTimeout(async function () {
      cm.template = await readFile(BASE_PATH + '/' + cm.indexTplFile, "utf8")
    }, 1000)
  } else {  
    cm.template = await readFile(BASE_PATH + '/' + cm.indexTplFile, "utf8")
  }
  
  try { // Inland locations only
    const fetchOpts = {
      headers: {
        'token': process.env.NOAA_KEY
      }
    }
    if(! getCached[CACHE_LOCATION, { lat: sm.lat, long: sm.long}] ) {
      sm.loc = await (await fetch(`https://api.weather.gov/points/${sm.lat},${sm.long}`, fetchOpts)).json()
      setCache(CACHE_LOCATION, { lat: sm.lat, long: sm.long}, sm.loc)
      sm.wfo = sm.loc.properties.gridId
      sm.x = sm.loc.properties.gridX
      sm.y = sm.loc.properties.gridY
      sm.relLoc = sm.loc.properties.relativeLocation.properties
    } else {
      sm.loc = getCached(CACHE_LOCATION, {lat: sm.lat, long: sm.long})
       _.log(`Using location cache. ${sm.relLoc.city}, ${sm.relLoc.state}`)
    }
    _.log(`Location aquired. ${sm.relLoc.city}, ${sm.relLoc.state}`)
  } catch (err) { 
    const msg = 'could not get location.'
    _.warn(msg, sm, err);
    throw err // Not swallowing this
  }
  
  try {
    sm.noaaUrl = `https://api.weather.gov/gridpoints/${sm.wfo}/${sm.x},${sm.y}/forecast?units=${cm.units}`
    sm.noaa = getCached(CACHE_FORECAST, sm.noaaUrl)
    if(! sm.noaa ) {
      sm.noaaRaw = await fetch(sm.noaaUrl)
      const noaa = await sm.noaaRaw.json()
      if(noaa.properties) {
        sm.noaa = setCache(CACHE_FORECAST, sm.noaaUrl, noaa)
      } else {
        const msg = 'Invalid NOAA response.'
        _.log([msg, noaa])
        throw new Error('Invalid NOAA response')
      }
      _.log('Using fresh NOAA forecast')
    } else if (sm.noaa && sm.noaa.properties) {
      _.log('Using our cached NOAA forecast')
    } else {
      throw new Error(
        'Error got BAD response from NOAA. ' +
        'This is probably due to using up our Quota, or a bad query.\n' +
        'Result: \n' + JSON.stringify(sm.noaaRaw) + '\n' +
        'Query: \n' + sm.noaaUrl
      )
    }
  } catch (err) { 
    const msg = 'Error getting response from NOAA.'
    _.warn([msg, err]) // log hard, we are swallowing
    return response.status(500).send(msg)
  }
  
  try {
    
    const catQuery = 'size=full'
    sm.cat = getCached(CACHE_CAT, catQuery)
    if (!sm.cat ) {
      const newKitty = await (await fetch('https://api.thecatapi.com/v1/images/search?'+catQuery, cm.catOpts)).json()
      sm.cat = setCache(CACHE_CAT, catQuery, newKitty)
      _.log('Updated catsCache, API key: '+ cm.catOpts.headers['x-api-key' ])
    }
    _.log(`We have ${getCacheLength(CACHE_CAT)} cats in the log`)
  } catch (err) { 
    _.warn('Could not get new cat!')
    sm.cat = [Object.assign({}, cm.catTemplate[0])]
  }
  
  try {
    const lastCatFile = getLastCached(CACHE_CAT_FILE) || {}
    sm.finalImageSrc = sm.cat[0].url || `${cm.baseUrl}${cm.catFilesDir}/${cm.defaultCat}`
    sm.finalImageSrcAlt = lastCatFile.url || `${cm.baseUrl}${cm.catFilesDir}/${cm.defaultCat}`
    const lastCatCallThrottled = lastCatFile.id === sm.cat[0].id
    // WARNING: This can take an ungodly 5 seconds.
    // Use memory? send as base64? use mem if available? gcafter 10 seconds?
    // _.log(request.headers)
    setTimeout(async () => {
      if (lastCatCallThrottled) {
        return _.log(['Redownload attempt of same cat stopped.', caches])
      }
      
      const catId = sm.cat[0].id
      const newCatFileName = `${catId}.jpg`
      const defaultCatFile = `${cm.basePublicDir}/${cm.catFilesDir}/${cm.defaultCat}`
      const newCatFile = `${cm.basePublicDir}/${cm.catFilesDir}/${newCatFileName}`
      const res = await Jimp.read(sm.cat[0].url)
      await res
        .scaleToFit(480, 272) // resize
        .quality(75) // set JPEG quality
        .write(newCatFile) // save
      // Storage is slow. takes a second to make files available
      // would be fun to test how long it takes to be ready.
      // fhe only tool we have is to probe it
      lastCatFile.file && copyFile(lastCatFile.file, defaultCatFile)
      // new copy wont be available instantly...
      setCache(CACHE_CAT_FILE, catId, {
        id: catId,
        url: `${cm.baseUrl}${cm.catFilesDir}/${newCatFileName}`,
        file: newCatFile
      })
      _.log('Added ' + newCatFileName + ' to the cat files cache.')
    }, 1400) // Run this in 1000 milliseconds, keep going
  } catch (err) {throw err}
  
  const periods = sm.noaa.properties.periods
  const vars = {
    'curse': sm.curse,
    'now-name': periods[0].name || 'Oh no',
    'now-short': periods[0].shortForecast || 'Down',
    'later-name': periods[1].name || 'NOAA',
    'later-short': periods[1].shortForecast || 'Up',
    'img-src': sm.finalImageSrc,
    'img-src-alt': sm.finalImageSrcAlt,
    date: getCuteDate(),
    iso8601: (new Date()).toISOString(),
    city: sm.relLoc.city,
    state: sm.relLoc.state
  }

  const body = Object.keys(vars).reduce((acc,v,i,a) => replaceSpan(acc,v,vars[v]), cm.template)
  response.send(body)
  _.log('Done. Time for SSID="' + sm.SSID + '"')
  console.timeEnd(sm.SSID)
});

// listen for requests :) 
const listener = app.listen(process.env.PORT, function() {
  _.log('Your app listens to port # ' + listener.address().port);
});

const replaceSpan = (T,key,value) => T.replace(new RegExp('<x>'+key+'<\/x>', 'g'), value)

// In ESM, this has no context, unless you use `new` to instantiate this module, even at build time!
const getCuteDate = () => {
  const date = new Date()
  const opts = {}
  opts.hour = "numeric"
  opts.hour12 = cm ? cm.hour12 : false
  opts.timeZone = cm ? cm.timeZone : 'America/Chicago'
  opts.dateStyle = 'full'
  
  const dateArr = new Intl.DateTimeFormat('en-US', opts).format(date).split(' ')
  const ordDoM = _.formatOrdinals(parseInt(dateArr[2]))
  const prettyDateArr = [dateArr[0], dateArr[1], ordDoM]
  const result = prettyDateArr.join(' ')
  // _.log(prettyDateArr)
  return result
}
const startTiming = (path) => {
  const startDate = new Date()
  const startTs = startDate.getTime()
  const seed = Math.random()
  const SSID = (''+(startTs / 1000 + seed)).split('.').join('')

  _.log(`Our State Map label for ${path} is ${SSID}`)
  console.time(SSID)
  return {startDate, startTs, seed, SSID}
}

// DBG
_.log(`We\'re Running on ${process.platform} ${process.arch}. PID # ${process.pid}`)
